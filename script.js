function password() {
  let inputFirst = document.querySelector(".input-password-first");
  let inputSecond = document.querySelector(".input-password-second");
  let button = document.querySelector(".btn");

  let eye = document.querySelectorAll(".eye");
  Array.from(eye).forEach((item) => {
    item.addEventListener("click", changeEye);
  });

  function changeEye() {
    this.classList.toggle("fa-eye");
    this.classList.toggle("icon-password");
    this.classList.toggle("fa-eye-slash");
    this.classList.toggle("icon-password");

    this.previousElementSibling.type === "password"
      ? (this.previousElementSibling.type = "text")
      : (this.previousElementSibling.type = "password");
  }

  let p = document.createElement("p");
  p.textContent = `Нужно ввести одинаковые значения`;
  p.style.cssText = `margin-top: 0;
      top:170px;
      position: absolute;
      color: red;
      `;

  button.addEventListener("click", () => {
    let valueFirstInput = inputFirst.value;
    let valueSecondInput = inputSecond.value;

    if (
      valueFirstInput === valueSecondInput &&
      valueFirstInput !== "" &&
      valueSecondInput !== ""
    ) {
      p.remove();
      setTimeout(() => {
        alert(`You are welcome`);
      }, 50);
    } else {
      button.before(p);
    }
  });
}
password();
